#!/bin/sh

# Project Group
# =============
#
# Builds a group of several Maven projects.
#

echo ">> Group '$1' ..."

GROUP_DIR="$PWD"

for TARGET in "$1/"* ; do
    ./build-project.sh "$TARGET"
done

echo "<< Group '$1'."

