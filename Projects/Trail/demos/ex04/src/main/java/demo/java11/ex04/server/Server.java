/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2022 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.java11.ex04.server;

import org.apache.catalina.startup.Tomcat;

import demo.util.Demo;

/**
 * The application starts a <i>Tomcat</i> WebServer with our Web application.
 * When the server is running, you can open a browser window and call
 * 
 * <pre>
 * 	<code>http://localhost:8080/demo/translate?wordInEnglish=rain</code>
 * </pre>
 * 
 * @author Franz Tost
 *
 */
public class Server {

	// constructors /////

	private Server() { }
	

	// methods /////

	private void runServer() {

		Demo.log("Running server ...");

		try {

			final Tomcat tomcat = new Tomcat();

			tomcat.setBaseDir("tomcat");
			tomcat.setPort(8080);

			tomcat.addWebapp("/demo", "demo");

			tomcat.start();
			tomcat.getServer().await();

		} // try
		catch (Exception e) {

			Demo.log(e);

		} // catch

		Demo.log("Finished.");

	}

	/**
	 * Runs the demo application.
	 * 
	 * @param args no arguments needed.
	 */
	public static void main(String[] args) {

		new Server().runServer();

	}

}
