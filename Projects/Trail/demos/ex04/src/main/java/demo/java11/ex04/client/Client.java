/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2022 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.java11.ex04.client;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpClient.Version;
import java.net.http.WebSocket;
import java.util.concurrent.CompletionStage;
import java.util.concurrent.CountDownLatch;

import demo.util.Demo;

/**
 * This demo shows you, how to connect to a WebSocket.
 * 
 * @author Franz Tost
 */
public class Client {

	// constructors /////

	private Client() { }
	

	// methods /////

	private void runClient() throws InterruptedException {

		Demo.logWithThread("Running client ...");

		final String wordInEnglish = Demo.input("Word in English");
		
		final CountDownLatch done = new CountDownLatch(1);
		
		final WebSocket.Listener listener = new WebSocket.Listener() {
			
			@Override
			public void onOpen(final WebSocket webSocket) {
				
				Demo.logWithThread("onOpen(...)");
				WebSocket.Listener.super.onOpen(webSocket);
				
			}

			@Override
			public CompletionStage<?> onText(
				final WebSocket    webSocket,
				final CharSequence data,
				final boolean      last
			) {
				
				Demo.logWithThread("onText(...) > %s", data);
				return WebSocket.Listener.super.onText(webSocket, data, last);
				
			}

			@Override
			public CompletionStage<?> onClose(
				final WebSocket webSocket,
				final int       statusCode,
				final String    reason
			) {
				
				Demo.logWithThread("onClose(...) > %d %s", statusCode, reason);
				done.countDown();
				
				return
					WebSocket.Listener.super
						.onClose(webSocket, statusCode, reason);
				
			}
			
		};
		
		final HttpClient client =
			HttpClient.newBuilder().version(Version.HTTP_2).build();
		
		final WebSocket webSocket =
			client.newWebSocketBuilder()
				.buildAsync(
					URI.create("ws://localhost:8080/demo/translate"),
					listener
				)
				.join();
		
		webSocket.sendText(wordInEnglish, true);
		webSocket.sendClose(WebSocket.NORMAL_CLOSURE, "ok");
		
		done.await();
		Demo.logWithThread("Finished.");

	}

	/**
	 * Runs the demo application.
	 * 
	 * @param args no arguments needed.
	 * @throws InterruptedException 
	 */
	public static void main(String[] args) throws InterruptedException {

		new Client().runClient();

	}

}
