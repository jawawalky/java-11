/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2022 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.java11.ex04.server;

import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.server.ServerEndpoint;

import demo.util.Demo;

/**
 * A service using WebSockets, which translates English words into German.
 * 
 * @author Franz Tost
 *
 */
@ServerEndpoint("/translate")
public class TranslatorService {

	// constructors /////

	public TranslatorService() {

		Demo.logWithThread("<init>");
		
	}

	// methods /////

	@OnOpen
	public void onOpen() {
		
		Demo.logWithThread("opened ...");
		
	}

	@OnClose
	public void onClose() {
		
		Demo.logWithThread("... closed");
		
	}

	@OnMessage
	public String onMessage(final String message) throws Exception {
		
		Demo.logWithThread("onMessage(%s)", message);
		
		final String wordInEnglish = message;
		
		String wordInGerman = null;
		
		switch (wordInEnglish) {
		case "rain": wordInGerman = "Regen";  break;
		case "sun":  wordInGerman = "Sonne";  break;
		case "wind": wordInGerman = "Wind";   break;
		case "snow": wordInGerman = "Schnee"; break;
		default: wordInGerman = "---";
		} // switch
		
		return wordInGerman;
		
	}

	@OnError
	public void onError(final Throwable e) {
		
		Demo.logWithThread("error: %s", e.getMessage());
		
	}

}
