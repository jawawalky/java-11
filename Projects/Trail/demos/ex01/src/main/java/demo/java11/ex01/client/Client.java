/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2022 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.java11.ex01.client;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpClient.Version;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.net.http.HttpResponse.BodyHandlers;

import demo.util.Demo;

/**
 * This demo shows you, how to make a synchronous request with
 * the new {@code HttpClient}.
 * 
 * @author Franz Tost
 */
public class Client {

	// constructors /////

	private Client() { }
	

	// methods /////

	private void runClient() {

		Demo.log("Running client ...");
		
		try {
			
			final String wordInEnglish = Demo.input("Word in English");
			
			final URI uri =
				URI.create(
					"http://localhost:8080/demo/translate?wordInEnglish=" +
					wordInEnglish
				);
			
			final HttpRequest request =
				HttpRequest.newBuilder().uri(uri).GET().build();
			
			final HttpClient client =
				HttpClient.newBuilder()
					.version(Version.HTTP_2).build();
			
			final HttpResponse<String> response =
				client.send(request, BodyHandlers.ofString());
			
			Demo.log("Response Status: %d", response.statusCode());
			Demo.log("Word in German: %s", response.body());
			
		} // try
		catch (IOException | InterruptedException e) {
			
			Demo.log(e);
			
		} // catch
		
		Demo.log("Finished.");

	}

	/**
	 * Runs the demo application.
	 * 
	 * @param args no arguments needed.
	 */
	public static void main(String[] args) {

		new Client().runClient();

	}

}
