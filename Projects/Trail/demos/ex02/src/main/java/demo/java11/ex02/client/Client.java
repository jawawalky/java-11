/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2022 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.java11.ex02.client;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpClient.Version;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.net.http.HttpResponse.BodyHandlers;
import java.util.concurrent.CompletableFuture;

import demo.util.Demo;

/**
 * This demo shows you, how to make an asynchronous request with
 * the new {@code HttpClient}.
 * 
 * @author Franz Tost
 */
public class Client {

	// constructors /////

	private Client() { }
	

	// methods /////

	private void runClient() {

		Demo.log("Running client ...");
		
		final String wordInEnglish = Demo.input("Word in English");
		
		final URI uri =
			URI.create(
				"http://localhost:8080/demo/translate?wordInEnglish=" +
				wordInEnglish
			);
		
		final HttpRequest request =
			HttpRequest.newBuilder().uri(uri).GET().build();
		
		final HttpClient client =
			HttpClient.newBuilder()
				.version(Version.HTTP_2).build();
		
		final CompletableFuture<HttpResponse<String>> future =
			client.sendAsync(request, BodyHandlers.ofString());
		
		future.thenAccept(response -> {
			Demo.log("Response Status: %d", response.statusCode());
			Demo.log("Word in German: %s", response.body());
		});
			
		Demo.log("Finished.");
		
		Demo.sleep(2000);    // <- Give response a chance to return.

	}

	/**
	 * Runs the demo application.
	 * 
	 * @param args no arguments needed.
	 */
	public static void main(String[] args) {

		new Client().runClient();

	}

}
