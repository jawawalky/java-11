/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2022 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.java11.ex03.client;

import java.net.URI;

import demo.util.Demo;

/**
 * This demo shows you, how to make a synchronous POST request with
 * the new {@code HttpClient}.
 * 
 * @author Franz Tost
 */
public class Client {

	// constructors /////

	private Client() { }
	

	// methods /////

	private void runClient() {

		Demo.log("Running client ...");
		
		final String wordInEnglish = Demo.input("Word in English");
		
		// TODO
		//
		//  o Create a 'BodyPublisher', which contains a line with
		//    the English word.
			
		final URI uri = URI.create("http://localhost:8080/demo/translate");
		
		// TODO
		//
		//  o Create a POST request with this URI and the 'BodyPublisher'.
		//
		//  o Create an HTTP client, which uses HTTP version 2.0.
		//
		//  o Send request.
		//
		//  o Evaluate response.
				
		Demo.log("Finished.");

	}

	/**
	 * Runs the demo application.
	 * 
	 * @param args no arguments needed.
	 */
	public static void main(String[] args) {

		new Client().runClient();

	}

}
