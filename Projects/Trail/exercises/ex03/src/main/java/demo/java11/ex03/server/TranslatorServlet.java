/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2022 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.java11.ex03.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * A servlet, which translates English words into German.
 * 
 * @author Franz Tost
 *
 */
public class TranslatorServlet extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
	
	// methods /////
	
	@Override
	public void doPost(
		final HttpServletRequest  request,
		final HttpServletResponse response
	) throws IOException, ServletException {
		
		// <- Reading values from request body.
		
		final BufferedReader reader =
			new BufferedReader(
				new InputStreamReader(request.getInputStream())
			);
		
		final String wordInEnglish = reader.readLine();
		String wordInGerman = null;
		
		switch (wordInEnglish) {
		case "rain": wordInGerman = "Regen";  break;
		case "sun":  wordInGerman = "Sonne";  break;
		case "wind": wordInGerman = "Wind";   break;
		case "snow": wordInGerman = "Schnee"; break;
		default: wordInGerman = "---";
		} // switch
		
		response.setContentType("text/plain");
		
		final PrintWriter out = response.getWriter();
		out.println(wordInGerman);
		out.flush();
		
	}
	
}
