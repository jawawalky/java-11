/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2022 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.java11.ex04.client;

import java.net.http.HttpClient;
import java.net.http.HttpClient.Version;
import java.net.http.WebSocket;
import java.util.concurrent.CountDownLatch;

import demo.util.Demo;

/**
 * This demo shows you, how to connect to a WebSocket.
 * 
 * @author Franz Tost
 */
public class Client {

	// constructors /////

	private Client() { }
	

	// methods /////

	private void runClient() throws InterruptedException {

		Demo.logWithThread("Running client ...");

		final String wordInEnglish = Demo.input("Word in English");
		
		final CountDownLatch done = new CountDownLatch(1);
		
		final WebSocket.Listener listener = new WebSocket.Listener() {
			
			@Override
			public void onOpen(final WebSocket webSocket) {
				
				Demo.logWithThread("onOpen(...)");
				WebSocket.Listener.super.onOpen(webSocket);
				
			}
			
			// TODO
			//
			//  o Implement according to the method 'onOpen(...)' the WebSocket
			//    methods
			//
			//      o 'onText(WebSocket, CharSequence, boolean)' and
			//      o 'onClose(WebSocket, int, String)'
			//
			//  o When a text arrives, then print it on the console.
			//
			//  o When the socket is closed, print the status code and
			//    the reason on the console and decrease the count-down-latch.

		};
		
		final HttpClient client =
			HttpClient.newBuilder().version(Version.HTTP_2).build();
		
		// TODO
		//
		//  o Create a 'WebSocket' with the 'HttpClient'.
		//    The URI of the WebSocket is
		//
		//      o 'ws://localhost:8080/demo/translate'
		//
		//  o Send the English word to the server endpoint.
		//
		//  o Close the WebSocket.
		
		done.await();
		Demo.logWithThread("Finished.");

	}

	/**
	 * Runs the demo application.
	 * 
	 * @param args no arguments needed.
	 * @throws InterruptedException 
	 */
	public static void main(String[] args) throws InterruptedException {

		new Client().runClient();

	}

}
