/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2022 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.java11.ex04.server;

import demo.util.Demo;

/**
 * A service using WebSockets, which translates English words into German.
 * 
 * @author Franz Tost
 *
 */
public class TranslatorService {
	
	// TODO
	//
	//  o The server endpoint should be '/translate'.
	//
	//  o Annotate the methods with the appropriate annotations of the package
	//    'javax.websocket'.

	// constructors /////

	public TranslatorService() {

		Demo.logWithThread("<init>");
		
	}

	// methods /////

	public void onOpen() {
		
		Demo.logWithThread("opened ...");
		
	}

	public void onClose() {
		
		Demo.logWithThread("... closed");
		
	}

	public String onMessage(final String message) throws Exception {
		
		Demo.logWithThread("onMessage(%s)", message);
		
		final String wordInEnglish = message;
		
		String wordInGerman = null;
		
		switch (wordInEnglish) {
		case "rain": wordInGerman = "Regen";  break;
		case "sun":  wordInGerman = "Sonne";  break;
		case "wind": wordInGerman = "Wind";   break;
		case "snow": wordInGerman = "Schnee"; break;
		default: wordInGerman = "---";
		} // switch
		
		return wordInGerman;
		
	}

	public void onError(final Throwable e) {
		
		Demo.logWithThread("error: %s", e.getMessage());
		
	}

}
