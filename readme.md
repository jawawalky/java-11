# Java 11

## What you Need

If you want to build and run the following projects, then you need

- JDK 11+
- Maven

## Building Projects

### Building a Single Project

If you want to build a single project, then do the following

1. Go to the project folder.
2. Open a terminal window.
3. Run `mvn install`.

There are some dependencies between the projects. All projects use the project *Utilities*. So before you build other projects, you should build *Utilities* first.

When you run projects, where a *JMS* server is needed, then you need to build the project *ArtemisMQ*.

### Building all Projects

On Linux you can build all projects by running the script `./build-all.sh`.

## Adding Project to IDE

### Eclipse

You can add a project to *Eclipse*.

1. Start *Eclipse*.
2. Go to the *Package Explorer*.
3. Right click and choose `Import ... > Existing Maven Projects`.
4. Browse and select the desired project.
5. Open `Advanced` and select `[name]` as `Name template`.
6. Press `Finish`.